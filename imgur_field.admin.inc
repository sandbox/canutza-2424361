<?php

/**
 * @file
 * Administration form for the imgur_field module.
 */


/**
 * Menu callback; Displays the administration settings for Imgur.
 */
function imgur_fields_settings() {
  $form = array();

  $form['imgur_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t("Configure Imgur login details"),
  );

  $form['imgur_settings']['imgur_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t("Imgur Client ID"),
    '#default_value' => variable_get('imgur_client_id', ''),
    '#required' => TRUE,
  );

  $form['imgur_settings']['imgur_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t("Imgur Client Secret"),
    '#default_value' => variable_get('imgur_client_secret', ''),
    '#required' => TRUE,
  );

  $form['imgur_settings']['imgur_settings_description'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . "The Imgur application details for your application can be found on the imgur website, " . l("here", 'http://imgur.com/account/settings/apps', array('attributes' => array('target' => '_blank'))) . '</p>',
  );

  if ($form['imgur_settings']['imgur_client_id']['#default_value'] != '') {
    $imgur_authorization_code = variable_get('imgur_authorization_code', '');
    if ($imgur_authorization_code == '') {
      $authorization_link_options = array(
        'query' => array(
          'client_id' => $form['imgur_settings']['imgur_client_id']['#default_value'],
          'response_type' => 'code',
          'state' => 'init',
        ),
        'attributes' => array(
          'target' => '_blank', // this can also be a popup
          'class' => 'imgur-auth-link',
        )
      );
      $form['imgur_settings']['imgur_authorize_application'] = array(
        '#type' => 'markup',
        '#markup' => '<p>' . l("Authorize your application", "https://api.imgur.com/oauth2/authorize?", $authorization_link_options) . '</p>',
      );
    }
    else {
      $form['imgur_settings']['imgur_authorize_code'] = array(
        '#type' => 'markup',
        '#markup' => '<p>' . t("Your authorization code is: ") . $imgur_authorization_code . '</p>',
      );
    }
  }


  return system_settings_form($form);
}

/**
 * Page callback for the Imgur callback authorization
 */
function imgur_field_auth_callback() {
  $output = '';
  $query_parameters = drupal_get_query_parameters();
  if (isset($query_parameters['code'])) {
    // THIS ONE DOESN'T EXPIRE, ONLY IMGUR TOKENS EXPIRE!!!
    $imgur_authorization_code = $query_parameters['code'];
    $output .= t("Your code is: ") . $imgur_authorization_code;
    variable_set('imgur_authorization_code' , $imgur_authorization_code);
  }
  else {
    // Maybe redirect user to the settings page.
    $output = t("You arrived here by mistake, nothing to see here, bye.");
  }

  return $output;
}
